import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BucketListComponent } from './components/bucket-list/bucket-list.component';
import { BucketViewComponent } from './components/bucket-view/bucket-view.component';
const routes: Routes = [
  { path: '', component: BucketListComponent},
  { path: 'bucketView', component: BucketViewComponent },
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
