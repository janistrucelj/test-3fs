import { Component, OnInit } from '@angular/core';
import { Bucket, DefaultService } from 'src/libs/api-client';
import { Router } from '@angular/router';

@Component({
  selector: 'app-bucket-list',
  templateUrl: './bucket-list.component.html',
  styleUrls: ['./bucket-list.component.scss']
})
export class BucketListComponent implements OnInit {
  buckets: Bucket[] = [];
  loading: boolean = false;
  showCreateBucketComponent: boolean = false;
  constructor(private defaultService: DefaultService, private router: Router) { }
  displayedColumns: string[] = [
    'name',
    'location'
  ];
  ngOnInit(): void {
    this.getBuckets();
  }
  getBuckets() {
    this.showCreateBucketComponent = false;
    this.loading = true;
    this.defaultService.defaultHeaders = this.defaultService.defaultHeaders.set('Authorization', 'Token e8af2823-68cd-447c-87f1-014e98c59e39');
    this.defaultService.bucketsGet().subscribe((data) => {
      this.buckets = data.buckets
      this.loading = false;
    });
  }
  onBucketClick(id: string) {
    this.router.navigate(['/bucketView', { id: id }]);
  }
}
