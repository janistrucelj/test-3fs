import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Location, DefaultService } from 'src/libs/api-client';
@Component({
  selector: 'app-bucket-create',
  templateUrl: './bucket-create.component.html',
  styleUrls: ['./bucket-create.component.scss'],
})
export class BucketCreateComponent implements OnInit {
  loading: boolean = false;
  locations: Location[];
  createBucketName: string;
  createBucketId: string;
  @Output('getBuckets') getBuckets: EventEmitter<any> = new EventEmitter();
  constructor(private defaultService: DefaultService) {}

  ngOnInit(): void {
    this.defaultService.defaultHeaders = this.defaultService.defaultHeaders.set(
      'Authorization',
      'Token e8af2823-68cd-447c-87f1-014e98c59e39'
    );
    this.getLocations();
  }
  getLocations() {
    this.loading = true;
    
    this.defaultService.locationsGet().subscribe((data) => {
      this.locations = data.locations;
      this.loading = false;
    });
  }
  addNewBucket() {
    this.defaultService
      .bucketsPost({
        name: this.createBucketName,
        location: this.createBucketId,
      })
      .subscribe(() => {
        this.getBuckets.emit();
      });
  }
}
