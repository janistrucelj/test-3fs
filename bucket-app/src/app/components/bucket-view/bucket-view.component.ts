import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Bucket, DefaultService, ModelObject } from 'src/libs/api-client';
import { ConfirmationDialogComponent } from '../confirmation-dialog/confirmation-dialog.component';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-bucket-view',
  templateUrl: './bucket-view.component.html',
  styleUrls: ['./bucket-view.component.scss'],
})
export class BucketViewComponent implements OnInit {
  constructor(
    private defaultService: DefaultService,
    private route: ActivatedRoute,
    private router: Router,
    public dialog: MatDialog
  ) {}
  dialogRef: MatDialogRef<ConfirmationDialogComponent>;
  displayedColumns: string[] = ['name', 'lastModified', 'size', 'action'];
  selectedId: string;
  bucket: Bucket;
  loading: boolean = false;
  title: string = '';
  objects: ModelObject[];

  ngOnInit(): void {
    this.loading = true;
    this.selectedId = String(this.route.snapshot.paramMap.get('id'));
    this.defaultService.defaultHeaders = this.defaultService.defaultHeaders.set(
      'Authorization',
      'Token e8af2823-68cd-447c-87f1-014e98c59e39'
    );
    this.getBucket();
  }
  uploadObject(event: any) {
    const file: File = event.target!.files[0];
    this.defaultService
      .bucketsBucketObjectsPostForm(this.selectedId, file)
      .subscribe((data) => {
        this.getBucket();
      });
  }

  removeObject(id: string) {
    this.dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      disableClose: false,
    });
    this.dialogRef.componentInstance.confirmMessage =
      'Do you really want to delete this object';

    this.dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.defaultService
          .bucketsBucketObjectsObjectDelete(this.selectedId, id)
          .subscribe((data) => {
            this.getBucket();
          });
      }
    });
  }

  getBucket() {
    this.loading = true;
    this.defaultService.bucketsBucketGet(this.selectedId).subscribe((data) => {
      this.bucket = data.bucket;
      this.title = this.bucket.name;
      this.defaultService
        .bucketsBucketObjectsGet(this.selectedId)
        .subscribe((data) => {
          this.objects = data.objects;
          this.loading = false;
        });
    });
  }

  objectsSize() {
    let sum: number = 0;
    if (this.objects) {
      this.objects.forEach((element) => {
        sum += element.size;
      });
    }
    return sum;
  }

  deleteBucket() {
    this.dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      disableClose: false,
    });
    this.dialogRef.componentInstance.confirmMessage =
      'Do you really want to delete this bucket?';

    this.dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.defaultService
          .bucketsBucketDelete(this.selectedId)
          .subscribe((data) => {
            this.router.navigate(['/']);
          });
      }
    });
  }
}
